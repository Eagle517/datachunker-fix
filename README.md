# DataChunker Fix
This DLL fixes a bug in DataChunker::alloc where it mistakenly allocates a 16kb block of memory for requests larger than 16kb.  This primarily prevents the game from crashing when a lot of functions are defined.

## Usage
Use the latest version of [RedBlocklandLoader](https://gitlab.com/Eagle517/redblocklandloader/-/releases) to easily load the DLL into the game.

The DLL works on its own.  When a call to DataChunker::alloc is made with a requested memory size greater than the max size, it will increase the max size to accommodate the requested memory size.

## Building
This DLL relies on [TSFuncs](https://gitlab.com/Eagle517/tsfuncs) which you will need to build first.  Building with CMake is straightforward, although it has only been setup for MinGW.  A toolchain has been included for cross-compiling on Linux.
