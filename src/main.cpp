#include "TSHooks.hpp"

BlFunctionDef(void *, __thiscall, DataChunker__alloc, ADDR *, int);
BlFunctionHookDef(DataChunker__alloc);

void *__fastcall DataChunker__allocHook(ADDR *pthis, void *blank, int size)
{
	int maxSize = *((int *)pthis + 1);
	if (size > maxSize) {
		int newSize = maxSize*2 + 8;

#if TSFUNCS_DEBUG
		BlPrintf("DataChunker requested size (%d) exceeds max size (%d); increasing max size to %d",
				size, maxSize, newSize);
#endif

		*((int *)pthis + 1) = newSize;
	}

	return DataChunker__allocOriginal(pthis, size);
}

bool init()
{
	BlInit;

	BlScanFunctionHex(DataChunker__alloc, "55 8B EC 6A FF 68 ? ? ? ? 64 A1 ? ? ? ? 50 83 EC 08 53 56 57 A1 ? ? ? ? 33 C5 50 8D 45 F4 64 A3 ? ? ? ? 8B D9 8B 3B");
	BlCreateHook(DataChunker__alloc);
	BlTestEnableHook(DataChunker__alloc);

	BlPrintf("%s (v%s-%s): init'd", PROJECT_NAME, PROJECT_VERSION, TSFUNCS_DEBUG ? "debug":"release");
	return true;
}

bool deinit()
{
	BlTestDisableHook(DataChunker__alloc);

	BlPrintf("%s: deinit'd", PROJECT_NAME);
	return true;
}

bool __stdcall DllMain(HINSTANCE hinstance, unsigned int reason, void *reserved)
{
	switch (reason) {
		case DLL_PROCESS_ATTACH:
			return init();
		case DLL_PROCESS_DETACH:
			return deinit();
		default:
			return true;
	}
}

extern "C" void __declspec(dllexport) __cdecl PROJECT_EXPORT(){}
